# StrainPedia

![](screenshot.png)

## What is this?

### Summary
This is my final project for the module [Web Based Information Systems](https://www.thm.de/organizer/modulhandbuecher/fb-06-mni/modulhandbuch-inf-bs-2010.html?view=subject_item&id=14) at the [University of Applied Sciences Mittelhessen](https://www.thm.de/site/).

StrainPedia is a little single-page web app to interactively explore over 2000 different cannabis strains.

Live version available at http://axlworld.de:5000.

### Concept
My inspiration for this app came from different websites like [Leafly](https://www.leafly.com/) and [Wikileaf](https://www.wikileaf.com/) which offer ways to search through cannabis strains and their information.

My goal is to create a *quick and easy* experience for the user. Search through the strains by different keys (name, effect, flavor) and receive various information and content of your selected strain.

Heard of a new strain? Look it up in the app and swipe through images and videos.

Having insomnia or seizures? Search for various medical effects and see what strains might help you.

This app uses three APIs in total:
- [Evan Busse's Strain API](https://strains.evanbusse.com/) (as database, new request every search)
- [Google's Custom Search API](https://developers.google.com/custom-search) (content for images)
- [YouTube Data API v3](https://developers.google.com/youtube/v3) (content for videos)

### Design
The layout of this app is very simple and consists of a few main components:
- Searchbar
- Strain list
- Strain content

Everything is displayed at once and in the user's view, thus scrolling is held to a minimum and actions like a new search or strain selection are *quick and easy*.

As for the color scheme, I'm using [Google's Material Design Color System](https://material.io/design/color/the-color-system.html).

Due to this app being about plants and plants mostly being green, I chose a nice calm green as primary color. The suggested complimentary color to it was a vibrant but soothing purple (basically just the primary color but 100% inverted), so I went with it.

These primary and complimentary colors are used for certain elements like buttons, lists and selection. Overall I tried to keep it as clean as possible, with a plain white background and horizontal lines to seperate content.

## Development

### About this project
This app is made with [Vue.js](https://vuejs.org/), a progressive JavaScript framework, and the [Vue-CLI](https://cli.vuejs.org/) to aid development.
Vue uses client-side rendering and components to make up the website.

[Bootstrap 5](https://v5.getbootstrap.com/) is also included for fast and simple designs.

Again, these are the used APIs, so you might want to look into them:
- [Evan Busse's Strain API](https://strains.evanbusse.com/) (as database, new request every search)
- [Google's Custom Search API](https://developers.google.com/custom-search) (content for images)
- [YouTube Data API v3](https://developers.google.com/youtube/v3) (content for videos)

Please acquire your own API-keys to use from their respective website.

### Start developing
First you need to install all prerequisites:
- Node.js 10+
- Node Packet Manager (npm)
- Vue-CLI

The following instructions are for use with Ubuntu 20.04:

1. Install Node.js and npm using the Node Version Manager (nvm): 
```
(install nvm)
sudo apt-get install curl 

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

(open new terminal)

(install node)
nvm install v12.18.3

nvm use v12.18.3

(install npm)
sudo apt-get install npm
```

2. Install the Vue-CLI:
```
npm install -g @vue/cli
```

3. Clone repository:
```
git clone https://git.thm.de/aohs43/strainpedia.git
```

4. Install dependencies:
```
npm install
```

You're done, ready to develop!

### Deployment
Compiles and hot-reloads for development:
```
npm run serve
```

Compiles and minifies for production:
```
npm run build
```

Lints and fixes files:
```
npm run lint
```